DOC=doc
DOCTEX=$(DOC).tex
TEXFILES=$(DOCTEX) content.tex
BIBFILES=my.bib
IMAGES=img.pdf

doc: $(DOC).pdf

clean:
	-rm $(DOC).pdf $(DOC).aux $(DOC).bbl $(DOC).blg $(DOC).log $(IMAGES)

$(IMAGES): %.pdf: %.svg
	inkscape --export-pdf=$@ $<

$(DOC).pdf: $(TEXFILES) $(BIBFILES) $(IMAGES)
	pdflatex $(DOCTEX)
	bibtex $(DOC).aux
	pdflatex $(DOCTEX)
	pdflatex $(DOCTEX)
